import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";
import { Responsive, WidthProvider } from "react-grid-layout";   
import GridCell from "modules/pe-grid-layout-module/views/sections";
import { GRID_HEIGHT_COEF, GRID_HEIGHT_FACTOR } from "modules/pe-grid-layout-module/views/GridLayoutView";
const ResponsiveReactGridLayout = WidthProvider(Responsive);

class GridContainer extends React.Component 
{
    constructor(props) {
        super(props);
        this.state = {
            currentBreakpoint: "lg",
            compactType: "vertical",
            mounted: false,
            layouts: props.layouts
        };
    }
    componentDidMount() {
        this.setState({ mounted: true });
        // setTimeout( () => this.onNewLayout(), 20 )
    }
    componentDidUpdate(prevProps, prevState) 
    {
      //  console.log( prevProps.layouts !== this.props.layouts )
      //  console.log( prevProps.layouts.lg[0].h, prevState.layouts.lg[0].h, this.props.layouts.lg[0].h, this.state.layouts.lg[0].h )
      if(prevProps.layouts !== this.props.layouts)
      {
        this.setState({ layouts: this.props.layouts })
      }
    } 

    onHide = (id, is_hide) => {
      
    }
    onRemoveFloat = (float_id) => {
      
    }
    onUpdateFloat = (data, float_id, section_id) => {
      
    }
    onClipboardCopy = (i, data) => 
    {

    }
    onClipboardPaste = (i) => { 

    }
    onWaypointEnter = (section_id) => {
      
    }
  
    onWaypointLeave = (section_id) => {
      
    }
  
    onFixedAdd = (data) => {
      
    } 
    generateDOM() {
        return this.props.layouts.lg.map( (elem, i) =>
        {
          const el = {
            ...elem,
              data : {
                ...elem.data,
                height: elem.h * GRID_HEIGHT_COEF() * GRID_HEIGHT_FACTOR()
            }          
          }
          return (
            <div key={i} className={` ${elem.heightType} `}>
              <GridCell 
                {...el}
                type={elem.current_type}
                background={{}} 
                section_id={elem.id}
                key={i}
                i={i}
                user={this.props.user}
                is_admin={false}
                is_edit={false}
                level={0}
                onHide={this.onHide}
                onRemoveFloat={this.onRemoveFloat}
                onUpdateFloat={this.onUpdateFloat}
                onClipboardCopy={this.onClipboardCopy}
                onClipboardPaste={this.onClipboardPaste}
                onWaypointEnter={this.onWaypointEnter}
                onWaypointLeave={this.onWaypointLeave}
                onFixedAdd={this.onFixedAdd}
                getHelp={this.getHelp}
              /> 
            </div>
          );
        });
    }
    onBreakpointChange = (breakpoint)  =>
    {
        this.setState({
          currentBreakpoint: breakpoint
        });
    }
    onCompactTypeChange = () =>
    {
        const { compactType: oldCompactType } = this.state;
        const compactType =
          oldCompactType === "horizontal"
            ? "vertical"
            : oldCompactType === "vertical"
              ? null
              : "horizontal";
        this.setState({ compactType });
    }
    onLayoutChange = (layout, layouts) => 
    {
      //console.log(layout, layouts)
      //if(!layout.data) return
      this.props.onLayoutChange(layout, layouts);
    }
    
    onNewLayout = () => {
        this.setState({
          layouts: { lg: this.props.onToggleLayout() }
        });
    }
    
    render() {
        return (
          <div> 
            <ResponsiveReactGridLayout
              {...this.props}
              layouts={this.props.layouts}
              onBreakpointChange={this.onBreakpointChange}
              onLayoutChange={this.onLayoutChange}
              // WidthProvider option
              measureBeforeMount={false}
              // I like to have it animate on mount. If you don't, delete `useCSSTransforms` (it's default `true`)
              // and set `measureBeforeMount={true}`.
              useCSSTransforms={this.state.mounted}
              compactType={this.state.compactType}
              preventCollision={!this.state.compactType}
              margin={[ this.props.gap, this.props.gap ]} 
              rowHeight={ GRID_HEIGHT_COEF() }
            >
              {this.generateDOM()}
            </ResponsiveReactGridLayout>
          </div>
        );
    }
} 

export default GridContainer